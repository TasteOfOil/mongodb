const connectDB = require('./mongodbUrl');
const cors = require('cors');
const express = require('express');
const app = express();
require('dotenv').config();
const Student = require('./schema');
const port = process.env.PORT || 3000;

const start = async ()=>{
    try{
        await connectDB(process.env.MONGO_URL);
        app.listen(port, ()=>{
            console.log(`listening on port:${port}`);
        });
    } catch(err){
        console.log(err);
    }

}

start();




app.use(express.json());
app.use(cors({
    origin:'*'
}));

const createStudent = async (req,res)=>{
    try{
        const student = await Student.create(req.body);
        res.status(201).json({student});
        console.log('Create Student');
    }
    catch(err){
        res.status(500).json({msg:err});
    }
    
}
const getStudent = async (req,res)=>{
    try{
        const students = await Student.find({});
        res.status(200).json({students});
        console.log('Get all students');
    }
    catch(err){
        res.status(500).json({msg:err});
    }
    
}

app.get('/get',getStudent);

app.post('/post',createStudent);